package bank.account;

public abstract class Account<AN,AH>{

public long bal;
public AN acno;
public AH name;

public Account(AN a,AH b,long c){
this.acno=a;
this.name=b;
this.bal =c;
}

public AN GetAcno(){
return this.acno;
}

public AH GetName(){
return this.name;
}

public long GetBal(){
return this.bal;
}

public void Deposit(long c){
this.bal += c;
}

public boolean Withdraw(long c){
if(this.bal < c){
System.out.print("\nCANNOT WITHDRAW FROM "+this.acno);
return false;
}
this.bal -= c;
return true;
}

public void Display(){
System.out.println("ACCOUNT NO IS : "+this.acno+" , USER NAME IS : "+this.name+" , BALANCE IS : "+this.bal);
}
}

