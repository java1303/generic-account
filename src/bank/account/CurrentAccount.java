package bank.account;

public class CurrentAccount<AN,AH> extends Account<AN,AH>{

public static final long minbal = 1000;
public static final long pen = 100;

public CurrentAccount(AN a,AH b,long c){
super(a,b,c);
}

public boolean Withdraw(long c){
if(!super.Withdraw(c)){
return false;
}
if(this.bal < this.minbal){
this.bal = this.bal - this.pen;
}
return true;
}
}
